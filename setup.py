import numpy as np
from setuptools import setup, find_packages, Extension, Command

extensions = [
    Extension("acfsa.types",
        sources=["acfsa/types.pyx"],
        language="c++"),
    Extension("acfsa.algorithm",
        sources=["acfsa/algorithm.pyx"],
        language="c++")
]

setup(
    name="acfsa",
    author="Cory Giles",
    author_email="mail@corygil.es",
    version="0.0.1",
    description="Aho-Corasick algorithm for fast multi-string matching",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3.2",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        ],
    license="AGPLv3+",
    packages=find_packages(),
    install_requires=["cython", "numpy"],
    ext_modules=extensions,
    include_dirs=np.get_include()
)
