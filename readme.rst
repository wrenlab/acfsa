=============================================
acfsa - Aho-Corasick implementation in Python
=============================================

See https://bitbucket.org/wrenlab/acfsa/wiki/Home for installation and usage information.

License: AGPLv3+
